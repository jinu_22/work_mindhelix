
//#define DEBUG

#ifdef DEBUG
  #define DEBUG_print(x)  Serial.print(x)
  #define DEBUG_print_dec(x)  Serial.print(x,DEC) 
  #define DEBUG_println(x)  Serial.println(x)
#else
  #define DEBUG_print(x)
  #define DEBUG_print_dec(x)
  #define DEBUG_println(x)
#endif

#include <SocketIOClient.h>
#include <Adafruit_CC3000.h>
#include <SPI.h>
#include <string.h>

#include <RF22.h>
#include <SoftwareSPI.h>
String nstring;
char i,a,state[0];
int state1=0;
SoftwareSPIClass Software_spi;
RF22 rf22(SS, 0, &Software_spi);

// Define CC3000 chip pins

#define ADAFRUIT_CC3000_IRQ   3
#define ADAFRUIT_CC3000_VBAT  6
#define ADAFRUIT_CC3000_CS    10

// WiFi network (change with your settings !)
#define WLAN_SSID       "mindhelix"        // cannot be longer than 32 characters!
#define WLAN_PASS       "D@ftPunk"
#define WLAN_SECURITY   WLAN_SEC_WPA2 // This can be WLAN_SEC_UNSEC, WLAN_SEC_WEP, WLAN_SEC_WPA or WLAN_SEC_WPA2
#define IDLE_TIMEOUT_MS  3000 

//objects
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT, SPI_CLOCK_DIV2); // you can change this clock speed
SocketIOClient client;

//server
char server[] = "x.sentinelsos.com";
uint32_t ip = cc3000.IP2U32(54,235,252,217);
int port = 80;

//files included
#include "buzzer.txt"
#include "wifi.txt";
#include "socket.txt"

void setup()
{
  Serial.begin(115200);
  InitializeCC30000();
  delay(1000);
  socket_setup();
  delay(2000);
   //================================================
  Software_spi.setPins(4, 5, 8);
  if (!rf22.init())
  DEBUG_println("RF22 init failed");
  pinMode(9, OUTPUT);
  
  alarm(3,50);
  delay(1000);
}
void loop(){
  client.monitor(cc3000);
  delay(100);
}
