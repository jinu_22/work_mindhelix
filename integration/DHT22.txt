#define DHTPIN 19     // what pin we're connected to

#define DHTTYPE DHT22   // DHT 22  (AM2302)

DHT dht(DHTPIN, DHTTYPE);

void dht_setup() {

  DEBUG_println("DHT setup!");
 
  dht.begin();
}

void dht_loop()
{
 
  h = dht.readHumidity();
  t = dht.readTemperature();

  // check if returns are valid, if they are NaN (not a number) then something went wrong!
  if (isnan(t) || isnan(h)) {
    DEBUG_println("Failed to read from DHT");
  } else {
    DEBUG_print("Humidity: "); 
    DEBUG_print(h);
    DEBUG_print(" %\t");
    DEBUG_print("Temperature: "); 
    DEBUG_print(t);
    DEBUG_println(" *C");
  } 
}
