void InitializeCC30000(void) {
 // Initialise the module 
 	DEBUG_println(("Initializing wifi..."));

if (!cc3000.begin())
{
  DEBUG_println(F("Couldn't begin()! Check your wiring?"));
  while(1);
}

// Optional SSID scan
redo:
if (!cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY)) {
  DEBUG_println(F("Failed!"));
  goto redo;
  //while(1);
	}

	//DEBUG_println(F("Connected!"));
	/* Wait for DHCP to complete */
	//DEBUG_println(F("Request DHCP"));
	while (!cc3000.checkDHCP())
	{ 
  		delay(100); // ToDo: Insert a DHCP timeout!
	}  
	//DEBUG_println("DHCP checked");
	DEBUG_println(F("Wifi_initialized!"));
}