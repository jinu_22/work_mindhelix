void dataArrived(SocketIOClient client, char *data) {
  DEBUG_println("Data Arrived: ");
  DEBUG_println(data);
   if(data[67] == 'n')
  {
    uint8_t data[] = "RELAYON"; 
    DEBUG_println("Sending to rf22_server");
    rf22.send(data, sizeof(data));
    rf22.waitPacketSent();
    
    // Now wait for a reply
    uint8_t buf[RF22_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);

    if (rf22.waitAvailableTimeout(500))
    { 
      // Should be a message for us now   
      if (rf22.recv(buf, &len))
      {
        DEBUG_print("got reply: ");
        DEBUG_println((char*)buf);
      }
      else
      
        DEBUG_println("recv failed");
      
    }
    else
      DEBUG_println("No reply, is rf22_server running?");
  }
  
  if(data[67] == 'f') 
  {
    uint8_t data[] = "RELAYOFF";
    DEBUG_println("Sending to rf22_server");
    rf22.send(data, sizeof(data));
    rf22.waitPacketSent();
    
    // Now wait for a reply
    uint8_t buf[RF22_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);

    if (rf22.waitAvailableTimeout(500))
    { 
      // Should be a message for us now   
      if (rf22.recv(buf, &len))
      {
        DEBUG_print("got reply: ");
        DEBUG_println((char*)buf);
      }
      else
      DEBUG_println("recv failed");
      
    }
    else
      DEBUG_println("No reply, is rf22_server running?");
      
      
  }
  

}

void socket_setup() {   
    client.setDataArrivedDelegate(dataArrived);

      //DEBUG_println("....new dataarriveddelegate....");
    retry:
    if (!client.connect(cc3000,"x.sentinelsos.com",80))
      {DEBUG_println(F("Not connected.")); //goto retry;
    }   

  delay(1000);

  DEBUG_println("Reached here");
}